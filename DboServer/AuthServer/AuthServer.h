#pragma once

#include "NtlSfx.h"
#include "NtlSharedType.h"
#include "NtlPacketEncoder_RandKey.h"
#include "mysqlconn_wrapper.h"

//Packets Go Here
#include <NtlPacketAM.h>
#include <NtlPacketMA.h>
//Client<->Auth
#include <NtlPacketAU.h>
#include <NtlPacketUA.h>

//Renderware Types
#include <rwplCore.h>
enum APP_LOG
{
	PRINT_APP = 2,
};
enum AUTH_SESSION
{
	SESSION_MASTER,
	SESSION_CLIENT,
	SESSION_SERVER_ACTIVE,
};
struct sSERVERCONFIG
{
	CNtlString		strClientAcceptAddr;
	WORD			wClientAcceptPort;
	CNtlString		strMasterAcceptAddr;
	WORD			wMasterAcceptPort;
	CNtlString		strMasterConnectAddr;
	WORD			wMasterConnectPort;
	CNtlString		ExternalIP;
	CNtlString		Host;
	CNtlString		User;
	CNtlString		Password;
	CNtlString		Database;
};

class CAuthServer;

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CClientSession : public CNtlSession
{
public:

	CClientSession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CNtlSession( SESSION_CLIENT )
	{
		SetControlFlag( CONTROL_FLAG_USE_SEND_QUEUE );

		if( bAliveCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_ALIVE );
		}
		if( bOpcodeCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_OPCODE );
		}

		SetPacketEncoder( &m_packetEncoder );
	}

	~CClientSession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendCharLogInReq(CNtlPacket * pPacket, CAuthServer * app);
	void						SendLoginDcReq(CNtlPacket * pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;

};

class CMasterSession : public CNtlSession
{
public:

	CMasterSession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CNtlSession(SESSION_MASTER)
	{
		SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);
		SetControlFlag(CONTROL_FLAG_USE_RECV_QUEUE);
		if (bAliveCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
		}
		if (bOpcodeCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
		}

		SetPacketEncoder(&m_packetEncoder);
	}

	~CMasterSession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendLoginReq(BYTE* pAuthKey, RwUInt32 accID, DWORD dwKey);
	void						SendNotifyMasterServer();
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;

};

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CAuthSessionFactory : public CNtlSessionFactory
{
public:

	CNtlSession * CreateSession(SESSIONTYPE sessionType)
	{
		CNtlSession * pSession = NULL;
		switch( sessionType )
		{
		case SESSION_CLIENT: 
			{
				pSession = new CClientSession;
			}
			break;
		case SESSION_MASTER:
			{
				pSession = new CMasterSession;
			}
			break;
		default:
			break;
		}

		return pSession;
	}
};

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CAuthServer : public CNtlServerApp
{
public:
	const char*		GetConfigFileHost()
	{
		return m_config.Host.c_str();
	}
	const char*		GetConfigFileUser()
	{
		return m_config.User.c_str();
	}
	const char*		GetConfigFilePassword()
	{
		return m_config.Password.c_str();
	}
	const char*		GetConfigFileDatabase()
	{
		return m_config.Database.c_str();
	}
	//For Multiple Server - Luiz45
	const string GetConfigFileEnabledMultipleServers()
	{
		return EnableMultipleServers.GetString();
	}
	const DWORD GetConfigFileMaxServers()
	{
		return MAX_NUMOF_SERVER;
	}

	const char*		GetConfigFileExternalIP()
	{
		std::cout << m_config.ExternalIP.c_str() << std::endl;
		return m_config.ExternalIP.c_str();
	}
	int	OnInitApp()
	{
		m_nMaxSessionCount = MAX_NUMOF_SESSION;

		m_pSessionFactory =  new CAuthSessionFactory;
		if( NULL == m_pSessionFactory )
		{
			return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
		}
		

		return NTL_SUCCESS;
	}
	//Just Check the Connection with Master Server each 5Seconds - Luiz45
	void CheckMasterServer()
	{
		//int rc = 0;
		////We are going to check if the socket for Master server is opened
		//CNtlSockAddr addr; // The address structure for a CNTL socket
		//sockaddr_in* realSocket;
		//addr.SetSockAddr(m_config.strMasterAcceptAddr.c_str(), htons(m_config.wMasterAcceptPort));
		//while (true)
		//{
		//	this->m_MasterSocket = new CNtlSocket();
		//	this->m_MasterSocket->Create(CNtlSocket::eSOCKET_TCP);
		//	rc = this->m_MasterSocket->Bind(addr);
		//	if (0 != rc)
		//	{
		//		this->m_MasterSocket->SetReuseAddr(true);
		//		realSocket = addr.operator sockaddr_in *();
		//		this->m_MasterSocket->Connect(realSocket);
		//		NTL_PRINT(PRINT_SYSTEM, "Still Connected Master Server");
		//		break;
		//	}
		//	else
		//	{
		//		rc = this->m_MasterSocket->Close();
		//		NTL_PRINT(PRINT_SYSTEM, "Failed to Connect Master Server");
		//	}
		//	Sleep(15000);//Will check each 5 Seconds if the server still connected
		//}
	}
	int	OnCreate()
	{
		int rc = NTL_SUCCESS;
		rc = m_clientAcceptor.Create(	m_config.strClientAcceptAddr.c_str(), m_config.wClientAcceptPort, SESSION_CLIENT, 
										MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT );
		if ( NTL_SUCCESS != rc )
		{
			return rc;
		}

		rc = m_network.Associate( &m_clientAcceptor, true );
		if( NTL_SUCCESS != rc )
		{
			return rc;
		}

		rc = m_MasterConnector.Create(m_config.strMasterConnectAddr.c_str(), m_config.wMasterConnectPort, SESSION_MASTER);
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_MasterAcceptor.Create(m_config.strMasterAcceptAddr.c_str(), m_config.wMasterAcceptPort, SESSION_MASTER,
			MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT);
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}


		rc = m_network.Associate(&m_MasterConnector, true);
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_MasterAcceptor, true);
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		return NTL_SUCCESS;

	}

	void	OnDestroy()
	{
	}

	int	OnCommandArgument(int argc, _TCHAR* argv[])
	{
		return NTL_SUCCESS;
	}

	int	OnConfiguration(const char * lpszConfigFile)
	{
		CNtlIniFile file;

		int rc = file.Create( lpszConfigFile );
		if( NTL_SUCCESS != rc )
		{
			return rc;
		}
		if( !file.Read("IPAddress", "Address", m_config.ExternalIP) )
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		//For multiple servers - Luiz45
		if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("ServerOptions", "MaxServerAllowed", MAX_NUMOF_SERVER))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (EnableMultipleServers.GetString() == "true")
		{
			for (int i = 0; i < MAX_NUMOF_SERVER; i++)
			{
				string strNm = "Char Server" + std::to_string(i);
				if (!file.Read(strNm.c_str(), "Address", ServersConfig[i][0]))
				{
					return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
				}
				if (!file.Read(strNm.c_str(), "Port", ServersConfig[i][1]))
				{
					return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
				}
			}
		}
 		if( !file.Read("Auth Server", "Address", m_config.strClientAcceptAddr) )
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("Auth Server", "Port",  m_config.wClientAcceptPort) )
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Auth Server", "MasterAddress", m_config.strMasterAcceptAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Auth Server", "MasterPort", m_config.wMasterAcceptPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "Address", m_config.strMasterConnectAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "Port", m_config.wMasterConnectPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("DATABASE", "Host",  m_config.Host) )
		{
			return NTL_ERR_DBC_HANDLE_ALREADY_ALLOCATED;
		}
		if( !file.Read("DATABASE", "User",  m_config.User) )
		{
			return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
		}
		if( !file.Read("DATABASE", "Password",  m_config.Password) )
		{
			return NTL_ERR_SYS_LOG_SYSTEM_INITIALIZE_FAIL;
		}
		if( !file.Read("DATABASE", "Db",  m_config.Database) )
		{
			return NTL_ERR_DBC_CONNECTION_CONNECT_FAIL;
		}
		return NTL_SUCCESS;
	}

	int	OnAppStart()
	{
		return NTL_SUCCESS;
	}

	void	Run()
	{
		DWORD dwTickCur, dwTickOld = ::GetTickCount();

		while( IsRunnable() )
		{		
			dwTickCur = ::GetTickCount();
			if( dwTickCur - dwTickOld >= 10000 )
			{
			//	NTL_PRINT(PRINT_APP, "Auth Server Run()");
				dwTickOld = dwTickCur;
			}
			Sleep(2);
		}
	}

	CMasterSession*			AcquireServerSession()
	{
		CNtlAutoMutex mutex(&m_serverMutex);
		mutex.Lock();

		if (NULL == m_MasterSession) return NULL;

		m_MasterSession->Acquire();
		return m_MasterSession;
	}

	void	SetServerSession(CMasterSession * pServerSession)
	{
		CNtlAutoMutex mutex(&m_serverMutex);
		mutex.Lock();

		if (NULL == pServerSession)
		{
			m_MasterSession->Release();
			m_MasterSession = pServerSession;
		}
		else
		{
			pServerSession->Acquire();
			m_MasterSession = pServerSession;
		}
	}

private:
	CNtlAcceptor				m_clientAcceptor;
	CNtlSocket*					m_MasterSocket;//For Master Server - Luiz45

	CNtlAcceptor				m_MasterAcceptor;
	CNtlConnector				m_MasterConnector;
	CNtlMutex					m_serverMutex;
	CMasterSession*				m_MasterSession;


	CNtlLog  					m_log;
	sSERVERCONFIG				m_config;
	DWORD						MAX_NUMOF_SERVER = 1;//This will be defined how many servers we can load
	CNtlString					EnableMultipleServers;//Added for enabling multiple server - Luiz45
	DWORD						MAX_NUMOF_GAME_CLIENT = 3;
	DWORD						MAX_NUMOF_SESSION = MAX_NUMOF_GAME_CLIENT + MAX_NUMOF_SERVER;
public:
	MySQLConnWrapper *			db;
	CNtlString					ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
};