#include "stdafx.h"
#include "NtlSfx.h"
/*Packets Needed*/
#include <NtlPacketAll.h>
#include "NtlResultCode.h"

#include "MasterServer.h"


/**--------------------------------------------------------------------------------------//
//								Session Handling										//
//--------------------------------------------------------------------------------------/*/

CNPCSession::~CNPCSession()
{
	//NTL_PRINT(PRINT_APP, "CAuthSession Destructor Called");
}


int CNPCSession::OnAccept()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	cout << "NPCServer  Connected" << endl;
	return NTL_SUCCESS;
}


void CNPCSession::OnClose()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();
}

int CNPCSession::OnDispatch(CNtlPacket * pPacket)
{
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);
	switch (pHeader->wOpCode)
	{
	case NM_HEARTBEAT:
		//CNPCSession::ReceiveHeartBeat(app);
		break;


	default:
		return CNtlSession::OnDispatch(pPacket);
	}

	return NTL_SUCCESS;
}


/**--------------------------------------------------------------------------------------//
//							Packets Handling Starts Here								//
//--------------------------------------------------------------------------------------*/

